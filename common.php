<?php
ob_start();
include ('../config/config.php');
$app = new Application();
init_config();

function makeLoginToken($userInfo){
    global $config;
	$system_pass = $config['settingSystem']['systemPassword'];
	return md5($userInfo['password'].$system_pass.$userInfo['userID']);
}

function isLogined() {
    if($_COOKIE['kodUserID']!='' && $_COOKIE['kodToken']!=''){
    	$user = systemMember::getInfo($_COOKIE['kodUserID']);
    	if(is_array($user) && isset($user['password'])) {
    	    if(makeLoginToken($user) == $_COOKIE['kodToken']){
        	    return true;
        	}
    	}
    }
    else {
        $kodLogin_ok = isset($_SESSION['kodLogin']) && in_array($_SESSION['kodLogin'], [1, '1', true, 'true']);
        $kod_ok = isset($_SESSION['kod']) && is_numeric($_SESSION['kod']);
        if($kodLogin_ok && $kod_ok) {
            $user = systemMember::getInfo($_SESSION['kod']);
            if(is_array($user) && isset($user['password'])) {
        	    return true;
        	}
        }
    }
    return false;
}

function tree($path, $exclude = array()) {
    $files = array();
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path)) as $splfileinfo) {
        if (!in_array(basename($splfileinfo), $exclude)) {
            $files[] = str_replace($path, '', '' . $splfileinfo);
        }
    }
    return $files;
}

function listFolderFiles($dir, $pattern, $tl)
{
    echo '<ol>';
    foreach (new DirectoryIterator($dir) as $fileInfo) {
        if (!$fileInfo->isDot()) {
            $not_output = (
                $fileInfo->isFile() &&
                !str_ends_with($fileInfo->getFilename(), '.html')
            );
            if(!$not_output) {
                echo '<li>';
                $text = $fileInfo->getFilename();
                if($fileInfo->isDir()) {
                    echo '<span>' . $text . '</span>';
                }
                else {
                    $href = preg_replace($pattern, '', $fileInfo->getPathname());
                    $title = array_get(substr($href, 1), $tl, null);
                    if(!is_null($title)) {
                        $text = $title;
                    }
                    echo '<a href="' . $href . '">' . $text . '</a>';
                }
                echo '</li>';
            }
            if ($fileInfo->isDir()) {
                listFolderFiles($fileInfo->getPathname(), $pattern, $tl);
            }
        }
    }
    echo '</ol>';
}

function get_var_dump($var) {
    ob_start();
    var_dump($var);
    $dump_info = ob_get_contents();
    ob_end_clean();
    return $dump_info;
}

if(!function_exists('str_starts_with')) {
    function str_starts_with($haystack, $needle) {
        return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
    }
}

 
if(!function_exists('str_ends_with')) {
    function str_ends_with($haystack, $needle) {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }
}

if(!function_exists('array_get')) {
    function array_get($key, $array, $default=null) {
        if(array_key_exists($key, $array)) {
            return $array[$key];
        }
        return $default;
    }
}