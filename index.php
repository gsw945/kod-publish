<?php
include('./common.php');

@ob_clean();
header('Content-Type: text/javascript;charset=utf-8');
if(isLogined()) {
    $markdown_js = dirname(__FILE__) . '/script.js';
    $markddown_js_c = file_get_contents($markdown_js);
    
    echo $markddown_js_c;
}
else {
    echo '/*login is required!*/';
}
@ob_flush();